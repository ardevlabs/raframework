<?php
namespace MILEXA\WPAWESOME;
if ( ! class_exists("MILEXA\\WPAWESOME\\WC_MWTable") ) :
class WC_MWTable extends \WP_List_Table  {

    public function __construct() {
        // Set parent defaults.
        parent::__construct([
            'singular' => 'category',     // Singular name of the listed records.
            'plural'   => 'categories',   // Plural name of the listed records.
            'ajax'     => false          // Does this table support ajax?
        ]);
    }

    public function get_columns() {
        $columns = [
            'cb'            => '<input type="checkbox" />', // Render a checkbox instead of text.
            'umeta_id'      => _x( 'ID', 'Column label', 'wp-list-table-example' ),
            'meta_value'    => _x( 'Name', 'Column label', 'wp-list-table-example' )
        ];
        return $columns;
    }

    protected function get_sortable_columns() {
        $sortable_columns = [
            'umeta_id'      => ['umeta_id', true],
            'meta_value'    => ['meta_value', false]
        ];
        return $sortable_columns;
    }

    protected function column_default( $item, $column_name ) {
        switch ( $column_name ) {
            case 'umeta_id':
            case 'meta_value':
                return $item[ $column_name ];
            default:
                return print_r( $item, true ); // Show the whole array for troubleshooting purposes.
        }
    }

    protected function column_cb( $item ) {
        return sprintf(
            '<input type="checkbox" name="%1$s[]" value="%2$s" />',
            $this->_args['singular'],        // Let's simply repurpose the table's singular label ("category").
            $item['umeta_id']                // The value of the checkbox should be the record's ID.
        );
    }

    protected function column_title( $item ) {
        $page = wp_unslash( $_REQUEST['page'] ); // WPCS: Input var ok.
        // Build edit row action.
        $edit_query_args = [
            'page'   => $page,
            'action' => 'edit',
            'category'  => $item['umeta_id'],
        ];
        $actions['edit'] = sprintf(
            '<a href="%1$s">%2$s</a>',
            esc_url( wp_nonce_url( add_query_arg( $edit_query_args, 'admin.php' ), 'editcategory_' . $item['umeta_id'] ) ),
            _x( 'Edit', 'List table row action', 'wp-list-table-example' )
        );
        // Build delete row action.
        $delete_query_args = [
            'page'   => $page,
            'action' => 'delete',
            'category'  => $item['umeta_id'],
        ];
        $actions['delete'] = sprintf(
            '<a href="%1$s">%2$s</a>',
            esc_url( wp_nonce_url( add_query_arg( $delete_query_args, 'admin.php' ), 'deletecategory_' . $item['umeta_id'] ) ),
            _x( 'Delete', 'List table row action', 'wp-list-table-example' )
        );
        // Return the title contents.
        return sprintf( '%1$s <span style="color:silver;">(id:%2$s)</span>%3$s',
            $item['meta_value'],
            $item['umeta_id'],
            $this->row_actions( $actions )
        );
    }

    protected function get_bulk_actions() {
        $actions = [
            'delete' => _x( 'Delete', 'List table bulk action', 'wp-list-table-example' ),
        ];
        return $actions;
    }

    protected function process_bulk_action() {
        // Detect when a bulk action is being triggered.
        if ( 'delete' === $this->current_action() ) {
            $post_ids = null;
            if(isset($_REQUEST['category'])) {
                $post_ids = array_map('intval', $_REQUEST['category']);
            }
            if(empty($post_ids)):
                return;
            else:
                foreach($post_ids as $id):
                    $this->deleteCategories($id);
                endforeach;
            endif;
        }
    }

    protected function deleteCategories($id){
        global $wpdb;
        $wpdb->delete("{$wpdb->prefix}usermeta", [
            'umeta_id' => $id,
            'user_id'  => 1
        ]);
        $ids = $this->RetreiveTemplateList($id);
        foreach($ids as $value):
            $wpdb->delete("{$wpdb->prefix}usermeta", [
                'meta_key' => "_template_content_{$value->id}",
                'user_id'  => 1
            ]);
        endforeach;
        $wpdb->delete("{$wpdb->prefix}usermeta", [
            'meta_key' => "_template_name_{$id}",
            'user_id'  => 1
        ]);
    }

    private static function RetreiveTemplateList($cat){
        global $wpdb;
        $prefix = $wpdb->prefix;
        $result = $wpdb->get_results("SELECT umeta_id id, meta_value name from {$prefix}usermeta where meta_key='_template_name_{$cat}' ");
        return $result;
    }

    public function prepare_items() {
        global $wpdb; 
        $per_page = 5;
        $columns  = $this->get_columns();
        $hidden   = [];
        $sortable = $this->get_sortable_columns();
        $this->_column_headers = [$columns, $hidden, $sortable];
        $this->process_bulk_action();
        $data = $this->getCategories();
        usort( $data, [$this, 'usort_reorder']);
        $current_page = $this->get_pagenum();
        $total_items = count( $data );
        $data = array_slice( $data, ( ( $current_page - 1 ) * $per_page ), $per_page );
        $this->items = $data;
        $this->set_pagination_args([
            'total_items' => $total_items,                     // WE have to calculate the total number of items.
            'per_page'    => $per_page,                        // WE have to determine how many items to show on a page.
            'total_pages' => ceil( $total_items / $per_page ), // WE have to calculate the total number of pages.
        ]);
    }

    protected function usort_reorder( $a, $b ) {
        // If no sort, default to title.
        $orderby = ! empty( $_REQUEST['orderby'] ) ? wp_unslash( $_REQUEST['orderby'] ) : 'umeta_id'; // WPCS: Input var ok.
        // If no order, default to asc.
        $order = ! empty( $_REQUEST['order'] ) ? wp_unslash( $_REQUEST['order'] ) : 'asc'; // WPCS: Input var ok.
        // Determine sort order.
        $result = strcmp( $a[ $orderby ], $b[ $orderby ] );
        return ( 'asc' === $order ) ? $result : - $result;
    }
    protected function getCategories(){
        global $wpdb;
        $sql    = "SELECT * FROM {$wpdb->prefix}usermeta where user_id=1 and meta_key='template_category_name' ";
        $result = $wpdb->get_results( $sql, 'ARRAY_A' );
        return $result;
    }

}
endif;