<?php
namespace MILEXA\WPAWESOME;

if ( ! class_exists("MILEXA\\WPAWESOME\\WC_MWShortCode") ) :
    class WC_MWShortCode
    {
        public function __construct() {
            add_action('init',        [$this, 'add_shortcodes']);
            add_filter('the_content', [$this, 'filter_eliminate_autop']);
            add_filter('widget_text', [$this, 'filter_eliminate_autop']);
        }

        public function filter_eliminate_autop( $content ) {
            $block = join( "|", self::getTags());

            // replace opening tag
            $content = preg_replace( "/(<p>)?\[($block)(\s[^\]]+)?\](<\/p>|<br \/>)?/", "[$2$3]", $content );

            // replace closing tag
            $content = preg_replace( "/(<p>)?\[\/($block)](<\/p>|<br \/>)/", "[/$2]", $content );
            return $content;
        }

        public function add_shortcodes() {
            $shortCodes = self::getAddonsShortcode();
            foreach ($shortCodes as $class => $shortcode ) {
                foreach($shortcode as $methods) {
                    $function_name  = $methods;
                    $tag            = str_replace("shortcode_", '', $methods);
                    $classname      = $class;
                    if(method_exists("{$classname}","{$function_name}")):
                        add_shortcode($tag, ["{$classname}", "{$function_name}"]);
                    endif;
                }
            }

            $appShortCodes = self::getAppsShortcode();
            foreach ($appShortCodes as $class => $shortcode ) {
                foreach($shortcode as $methods) {
                    $function_name  = $methods;
                    $tag            = str_replace("shortcode_", '', $methods);
                    $classname      = $class;
                    if(method_exists("{$classname}","{$function_name}")):
                        add_shortcode($tag, ["{$classname}", "{$function_name}"]);
                    endif;
                }
            }
        }

        protected static function getAddonsShortcode(){
            $dirs      = glob(AA_PATH .'vendor/ardevlabs/*-addon', GLOB_MARK);
            $shortcode = null;
            $class_methods  = [];
            if(is_array($dirs) && count($dirs) > 0):
                foreach ($dirs as $dir) {
                if (is_dir($dir)) {
                    $path        = AA_PATH . 'vendor/ardevlabs/' . basename($dir) . "/src/";
                    $setting     = self::read($path."setting.json");
                    if($setting["active"]):
                        if($setting["type"] == "addon"):
                            $shortcode   = ($setting["shortcode"]);
                            self::need($path.$shortcode['path']);
                            $shortcodeClass = $shortcode['class'];
                            if(class_exists("{$shortcodeClass}")){
                                if(method_exists("{$shortcodeClass}","init")):
                                    call_user_func(["{$shortcodeClass}", 'init']);
                                    foreach(get_class_methods(new $shortcodeClass()) as $method){
                                        $class_methods[$shortcodeClass][] = $method;
                                    }
                                else:
                                    wp_die("Cannot Load method: {$shortcodeClass}::init()");
                                endif;
                            }else{
                                wp_die("Cannot Load Addon ShortCode");
                            }
                        endif;
                    endif;
                }
            }
            endif;
            return $class_methods;
        }

        protected static function getAppsShortcode(){
            $dirs      = glob(AA_PATH .'apps/*', GLOB_MARK);
            $shortcode = null;
            $class_methods  = [];
            if(is_array($dirs) && count($dirs) > 0):
                foreach ($dirs as $dir) {
                if (is_dir($dir)) {
                    $path        = AA_PATH . 'apps/' . basename($dir) . "/";
                    $setting     = self::read($path."setting.json");
                    if($setting["active"]):
                        $shortcode   = ($setting["shortcode"]);
                        self::need($path.$shortcode['path']);
                        $shortcodeClass = $shortcode['class'];
                        if(class_exists("{$shortcodeClass}")){
                            if(method_exists("{$shortcodeClass}","init")):
                                call_user_func(["{$shortcodeClass}", 'init']);
                                foreach(get_class_methods(new $shortcodeClass()) as $method){
                                    $class_methods[$shortcodeClass][] = $method;
                                }
                            else:
                                wp_die("Cannot Load method: {$shortcodeClass}::init()");
                            endif;
                        }else{
                            wp_die("Cannot Load Addon ShortCode");
                        }
                    endif;
                }
            }
            endif;
            return $class_methods;
        }

        protected static function getTags(){
            $data       = [];
            $shortCodes = self::getAddonsShortcode();
            foreach ($shortCodes as $shortcode ) {
                foreach($shortcode as $methods){
                    $data[]   = str_replace("shortcode_", '',$methods);
                }
            }
            return $data;
        }

        protected static function need($path){
            if(file_exists($path))
                return require_once $path;
        }
        protected static function read($string){
            $autoload = self::convert(file_get_contents($string));
            $autoload = $autoload["system"]["autoload"][0];
            return $autoload;
        }
        protected static function convert($string){
            $data = json_decode($string,true);
            return $data;
        }
    }
endif;