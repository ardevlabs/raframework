<?php
namespace MILEXA\WPAWESOME;
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists("MILEXA\\WPAWESOME\\WC_MWMenu") ) :
    class WC_MWMenu {
        public function __construct(){
            add_action('admin_menu', [$this , 'RegisterAdminMenu']);
        }

        public function RegisterAdminMenu() {
            /* Menu Position
             * 5 - below Posts
             * 10 - below Media
             * 15 - below Links
             * 20 - below Pages
             * 25 - below comments
             * 60 - below first separator
             * 65 - below Plugins
             * 70 - below Users
             * 75 - below Tools
             * 80 - below Settings
             * 100 - below second separator
             */
            $this->RegisterAdminSubMenu();
        }
        public function RegisterAdminSubMenu(){
            self::loadMenuAddons();
            self::loadMenuApps();
        }
        protected static function loadMenuAddons(){
            $dirs = glob(AA_PATH .'vendor/ardevlabs/*-addon', GLOB_MARK);
            $menu = null;
            if(is_array($dirs) && count($dirs) > 0):
            foreach ($dirs as $dir) {
                if (is_dir($dir)) {
                    $path        = AA_PATH . 'vendor/ardevlabs/' . basename($dir) . "/src/";
                    $setting     = self::read($path."setting.json");
                    if($setting["active"]):
                        if($setting["type"] == "addon"):
                            $menu   = ($setting["menu"]);
                            $menuClass = $menu['class'];
                            if(class_exists("{$menuClass}")){
                                call_user_func(["{$menuClass}", 'load_menu']);
                                add_action('admin_head', ["{$menuClass}", "removeFirstMenu"]);
                            }else{
                                wp_die("Cannot Load Addon Menu");
                            }
                        endif;
                    endif;
                }
            }
            endif;
        }

        protected static function loadMenuApps(){
            $dirs = glob(AA_PATH .'apps/*', GLOB_MARK);
            $menu = null;
            if(is_array($dirs) && count($dirs) > 0):
                foreach ($dirs as $dir) {
                if (is_dir($dir)) {
                    $path        = AA_PATH . 'apps/' . basename($dir) . "/";
                    $setting     = self::read($path."setting.json");
                    if($setting["active"]):
                        $menu   = ($setting["menu"]);
                        self::need($path.$menu['path']);
                        $menuClass = $menu['class'];
                        if(class_exists("{$menuClass}")){
                            call_user_func(["{$menuClass}", 'load_menu']);
                            add_action('admin_head', ["{$menuClass}", "removeFirstMenu"]);
                        }else{
                            wp_die("Cannot Load Addon Menu");
                        }
                    endif;
                }
            }
            endif;

        }

        protected static function need($path){
            if(file_exists($path))
                return require_once $path;
        }
        protected static function read($string){
            $autoload = self::convert(file_get_contents($string));
            $autoload = $autoload["system"]["autoload"][0];
            return $autoload;
        }
        protected static function convert($string){
            $data = json_decode($string,true);
            return $data;
        }
    }
endif;