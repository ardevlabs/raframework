<?php
namespace MILEXA\WPAWESOME;

if ( ! class_exists("MILEXA\\WPAWESOME\\WC_MWViews") ) :
    class WC_MWViews
    {
        public function __construct(){}

        public static function makeAddonView($view,$data=null){
            if(is_array($view)){
                $name       = str_replace('.', '/', $view["view"]);
                $AddonName  = $view["addon"];
                $link = AA_PATH. "vendor/ardevlabs/".$AddonName."/src/views/" . $name . ".php";
                if (file_exists($link)) {
                    self::assignData($data);
                    self::need($link);
                }else{
                    wp_die("view not found");
                }
            }
        }

        public static function makeAppView($view,$data=null){
            if(is_array($view)){
                $name       = str_replace('.', '/', $view["view"]);
                $AddonName  = $view["addon"];
                $link = AA_PATH. "apps/".$AddonName."/views/" . $name . ".php";
                if (file_exists($link)) {
                    self::assignData($data);
                    self::need($link);
                }else{
                    wp_die("view not found");
                }
            }
        }

        protected static function assignData($data){
            global $SF_DATA_VIEW;

            if(!is_null($data)):
                $array = [];
                foreach ($data as $key => $value):
                    $array[$key] = $value;
                endforeach;
                $SF_DATA_VIEW = $array;
            endif;
        }
        protected static function need($path){
            if(file_exists($path))
                return require_once $path;
        }
        protected static function read($string){
            $autoload = self::convert(file_get_contents($string));
            $autoload = $autoload["system"]["autoload"][0];
            return $autoload;
        }
        protected static function convert($string){
            $data = json_decode($string,true);
            return $data;
        }
    }
endif;