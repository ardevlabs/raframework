<?php
namespace MILEXA\WPAWESOME;

if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists("MILEXA\\WPAWESOME\\WC_MWCore") ) :
    class WC_MWCore
    {
        /**
         * WC_MWCore constructor.
         */
        public function __construct() {
            $this->init();
        }
        public function init(){
            new WC_MWMenu();
            new WC_MWShortCode();

            $this->loadAddons();
            $this->loadApps();
            add_action('admin_menu',                         [$this , 'remove_menus']);
            add_action('admin_bar_menu',                     [$this , 'remove_wp_logo'], 999 );
            add_action('admin_init',                         [$this, 'sf_redirect']);

            add_filter('plugin_action_links_' . AA_BASENAME, [$this, 'plugin_settings_link']);
            add_filter('plugin_row_meta',                    [$this ,'aa_plugin_meta_links'], 10, 2);
            add_filter('admin_footer_text',                  '__return_empty_string', 11 );
            add_filter('update_footer',                      '__return_empty_string', 11 );
        }

        public static function loadAddons(){
            $dirs = glob(AA_PATH.'vendor/ardevlabs/*-addon', GLOB_MARK);
            if(is_array($dirs) && count($dirs) > 0):
                foreach ($dirs as $dir) {
                if (is_dir($dir)) {
                    $path        = AA_PATH . 'vendor/ardevlabs/' . basename($dir) . "/src/";
                    $setting     = self::read($path."setting.json");
                    if($setting["active"]):
                        if($setting["type"] == "addon"):
                            $boostrap       = ($setting["boostrap"]);
                            $Config         = ($setting["Config"]);
                            $mainClass      = $boostrap['class'];
                            $configClass    = $Config['class'];

                            if(class_exists("{$mainClass}") && class_exists("{$configClass}")){
                                call_user_func(["{$configClass}", 'init']);
                                call_user_func(["{$mainClass}", 'init']);
                            }

                            $models = self::fetch($dir . '/models');
                            if(is_array($models) && count($models)>0):
                                foreach ($models as $v):
                                    self::need($v);
                                endforeach;
                            endif;
                        else:
                            $Config         = ($setting["Config"]);
                            $configClass    = $Config['class'];

                            if(class_exists("{$configClass}")){
                                call_user_func(["{$configClass}", 'init']);
                            }
                        endif;
                    endif;
                }
            }
            endif;
        }
        public static function loadApps(){
            $dirs = glob(AA_PATH.'apps/*', GLOB_MARK);
            if(is_array($dirs) && count($dirs) > 0):
                foreach ($dirs as $dir) {
                if (is_dir($dir)) {
                    $path        = AA_PATH . 'apps/' . basename($dir) . "/";
                    $setting     = self::read($path."setting.json");
                    if($setting["active"]):
                        $boostrap   = ($setting["boostrap"]);
                        $Config     = ($setting["Config"]);
                        self::need($path.$boostrap['path']);
                        self::need($path.$Config['path']);
                        $mainClass   = $boostrap['class'];
                        $configClass = $Config['class'];
                        if(class_exists("{$mainClass}") && class_exists("{$configClass}")){
                            call_user_func(["{$configClass}", 'init']);
                            call_user_func(["{$mainClass}", 'init']);
                        }
                        $models = self::fetch($dir . '/models');
                        if(is_array($models) && count($models)>0):
                            foreach ($models as $v):
                                self::need($v);
                            endforeach;
                        endif;
                    endif;
                }
            }
            endif;
        }

        /**
         *
         */
        public static function install() {
            add_option('wc_sf_activation_redirect', true);
        }

        /**
         *
         */
        public function sf_redirect() {
            if (get_option('wc_sf_activation_redirect')) {
                delete_option('wc_sf_activation_redirect');
                if(!isset($_GET['activate-multi'])) {
                    wp_redirect(admin_url( 'admin.php?page=wc-settings&tab=wcext&section'));
                }
            }
        }

        /**
         *
         */
        public static function uninstall() {}

        /**
         * @param $links
         *
         * @return mixed
         */
        public function plugin_settings_link($links) {
            $url            = get_admin_url() . 'admin.php?page=aa.setting';
            $activated_url  = get_admin_url() . 'admin.php?page=aa.activation';
            $settings_link[] = '<a href="'.$url.'">' . __( 'Settings', 'textdomain' ) . '</a>';
            $settings_link[] = '<a href="'.$activated_url.'">' . __( 'Actived', 'textdomain' ) . '</a>';
            array_unshift( $links, $settings_link[1] );
            array_unshift( $links, $settings_link[0] );
            return $links;
        }

        /**
         * @param $links
         * @param $file
         *
         * @return array
         */
        public function aa_plugin_meta_links( $links, $file ) {
            if ( $file === AA_BASENAME.'/'.AA_BASENAME.'.php' ) {
                $links[] = '<a title="' . __( 'Pro Version' ) . '"><strong style="color:green">' . __( '[Pro Version]' ) . '</strong></a>';
            }
            return $links;
        }

        /**
         *
         */
        public function remove_menus(){
            if(!current_user_can('administrator')){
                remove_menu_page('index.php' );
                remove_menu_page('edit.php');
                remove_menu_page('upload.php');
                remove_menu_page('tools.php');
            }
        }

        /**
         * @param $wp_admin_bar
         */
        public function remove_wp_logo($wp_admin_bar) {
            $wp_admin_bar->remove_node( 'wp-logo' );
        }

        /**
         * @param $pattern
         *
         * @return array
         */
        protected static function fetch($pattern){
            return glob($pattern.'/*.php');
        }

        /**
         * @param $path
         *
         * @return bool|mixed
         */
        protected static function need($path){
            if(file_exists($path)):
                return require $path;
            else:
                return false;
            endif;
        }


        /**
         * @param $string
         *
         * @return array|mixed|object
         */
        protected static function read($string){
            $autoload = self::convert(file_get_contents($string));
            $autoload = $autoload["system"]["autoload"][0];
            return $autoload;
        }

        /**
         * @param $string
         *
         * @return array|mixed|object
         */
        protected static function convert($string){
            $data = json_decode($string,true);
            return $data;
        }
    }
endif;